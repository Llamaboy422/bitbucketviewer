abstract class LoginRepository{
  void login(String username, String password);
  Map<String, String> getHeaders();
}