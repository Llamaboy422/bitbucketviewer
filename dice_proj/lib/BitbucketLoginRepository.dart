import 'LoginRepository.dart';
import 'dart:convert';
import 'dart:io';

class BitbucketLoginRepository extends LoginRepository{
  String _bitBucketURL = "https://bitbucket.hylandqa.net/rest/api/1.0/projects";
  String _sessionToken;


  @override
  void login(String username, String password) {
    //Create base64 string of username and password
    String decodedAuth = "$username:$password";
    var bytes = UTF8.encode(decodedAuth);
    String encodedAuth = BASE64.encode(bytes);
    Map<String, String> headers = getHeaders();
    headers["Authorization"] = "Basic $encodedAuth";
    
    HttpClient httpClient = new HttpClient();
    httpClient.getUrl(Uri.parse(_bitBucketURL))
      .then((HttpClientRequest request) {
      headers.forEach((key, value) =>
        request.headers.add(key, value));
        return request.close();
      }).then((HttpClientResponse response) {
        _sessionToken = response.cookies.removeAt(0).value;
      });
  }

  @override
  Map<String, String> getHeaders() {
    Map<String, String> headers = new Map<String, String>();
    headers["Content-Type"] = "application/json";
    return headers;
  }

  String getSessionToken(){
    return _sessionToken;
  }

}